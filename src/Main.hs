module Main where
import System.Environment
import Text.Parsec.String (parseFromFile)
import Lam

main :: IO ()
main = do
    a <- getArgs
    case a of
      [str] -> do
        result <- parseFromFile (lexeme term) str
        case result of
          Left _ -> error "Parse error"
          Right t -> print $ eval (toCanonical t)
      _ -> error "please pass one argument with the file containing the text to parse"