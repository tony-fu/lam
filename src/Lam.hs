module Lam where
import Text.Parsec.String (Parser)
import Text.Parsec (parseTest, parse)
import Text.Parsec.Char (oneOf, char, digit, letter, satisfy, space, endOfLine)
import Text.Parsec.Combinator (many1, chainl1, sepEndBy1, sepBy1)
import Control.Applicative ((<$>), (<*>), (<*), (*>), (<|>), many, (<$))
import Control.Monad (void, ap)
import Data.List (find, elemIndex, nub)
import Data.Maybe (fromJust)
import System.Environment

-- A lambda calculus language

lexeme :: Parser a -> Parser a
lexeme p = p <* (many (oneOf " \t"))

data Term =
  Var String
  | Abs Term Term
  | App Term Term
  | LetExp Term Term deriving (Eq)

data Decl = Decl String Term deriving (Eq)

instance Show Term where
  show = lambdaRep

instance Show Decl where
  show (Decl name term) = name ++ "=" ++ show term

lambdaRep :: Term -> String
lambdaRep (Var v) = v
lambdaRep (Abs h b) =
  "(\\"
  ++ (lambdaRep h)
  ++ "."
  ++ (lambdaRep b)
  ++ ")"
lambdaRep (App x y) = lambdaRep x ++ " " ++ lambdaRep y

var :: Parser Term
var = Var <$> many1 letter

value :: Parser Term
value = var <|> abstract

heads :: Parser [Term]
heads = sepBy1 var (lexeme (char ','))

-- letexp :: Parser Term
-- letexp = do
--   void $ string "let"


abstract :: Parser Term
abstract = do
  void $ lexeme $ char '{'
  hs <- lexeme heads
  void $ lexeme $ char '|'
  b <- lexeme term
  void $ char '}'
  return (go hs b)
  where
    go :: [Term] -> Term -> Term
    go [h] b = Abs h b
    go (h:hs) b = Abs h (go hs b)

term :: Parser Term
term = do
  vs <- sepEndBy1 value (many1 (oneOf " \t"))
  return $ go vs
  where
    go :: [Term] -> Term
    go [v] = v
    go (v1:v2:vr) = go (App v1 v2:vr)

decl :: Parser Decl
decl = do
  iden <- lexeme (many1 letter)
  void $ lexeme $ char '='
  t <- lexeme term
  return $ Decl iden t

decls :: Parser [Decl]
decls = sepEndBy1 decl (many1 endOfLine)

test = do
  putStr "[Test] Var: "
  parseTest (lexeme term) "a "
  putStr "[Test] Application: "
  parseTest (lexeme term) "a b "
  putStr "[Test] Abstraction: "
  parseTest (lexeme term) "{a|a}"
  putStr "[Test] Relax space in head: "
  parseTest (lexeme term) "{ a| a b c }"
  putStr "[Test] Relax space in body: "
  parseTest (lexeme term) "{a| a b }"
  putStr "[Test] Nested Abstraction: "
  parseTest (lexeme term) "{a|{b|c}} "
  putStr "[Test] Abs App: "
  parseTest (lexeme term) "{a|a} b"
  putStr "[Test] Combinations: "
  parseTest (lexeme term) "{a|a b {c|c} d}"
  putStr "[Test] Multiple heads: "
  parseTest (lexeme term) "{ a, b | a b { c | c } d } a b c"
  putStr "[Test] Declaration: "
  parseTest (lexeme decl) "x = { a, b | a b { c | c } d } a b c"
  putStr "[Test] Multi Declaration: "
  parseTest (lexeme decls) "x = {a|a}\ny = x b"
  testCanonicalize


parseTerm str =
  case parse term "" str of
    Left _ -> error "Parse error"
    Right t -> t

-- Nameless Canonical Form
data NamelessTerm =
  CanonicalVar Int Term
  | CanonicalAbs NamelessTerm Term
  | CanonicalApp NamelessTerm  NamelessTerm Term deriving (Eq)

instance Show NamelessTerm where
  show (CanonicalVar v ext) = show ext
  show (CanonicalAbs t ext) = show ext
  show (CanonicalApp t1 t2 ext) = show ext

freeVar :: Term -> [String]
freeVar (Var x) = [x]
freeVar (Abs (Var x) t) = filter (/= x) (freeVar t)
freeVar (App t1 t2) = nub (freeVar t1 ++ freeVar t2)

canonical :: Term -> [String] -> NamelessTerm
canonical ext@(Var x) scope =
  case (x `elemIndex` scope) of
    Nothing -> error $ x ++ " is not in the context"
    Just n -> CanonicalVar n  ext
canonical ext@(App t1 t2) scope = CanonicalApp (canonical t1 scope) (canonical t2 scope) ext
canonical ext@(Abs (Var x) t) scope = CanonicalAbs (canonical t (x:scope)) ext

toCanonical t = canonical t (freeVar t)

shift :: Int -> Int -> NamelessTerm -> NamelessTerm
shift c d (CanonicalVar k ext) = CanonicalVar (if k < c then k else (k + d)) ext
shift c d (CanonicalApp t1 t2 ext) = CanonicalApp (shift c d t1) (shift c d t2) ext
shift c d (CanonicalAbs t ext) = CanonicalAbs (shift (succ c) d t) ext

substitute :: Int -> NamelessTerm -> NamelessTerm -> NamelessTerm
substitute j s (CanonicalVar k ext) = if k == j  then s else CanonicalVar k ext
substitute j s (CanonicalApp t1 t2 ext) = CanonicalApp (substitute j s t1) (substitute j s t2) ext
substitute j s (CanonicalAbs t ext) = CanonicalAbs (substitute (succ j) (shift 0 1 s) t) ext


subTop :: NamelessTerm -> NamelessTerm -> NamelessTerm
subTop s t = shift 0 (-1) (substitute 0 (shift 0 1 s) t)

evalStep :: NamelessTerm -> Maybe NamelessTerm
evalStep (CanonicalApp (CanonicalAbs t1 _) s@(CanonicalAbs _ _) _) = Just $ subTop s t1
evalStep (CanonicalApp (CanonicalAbs t1 _) s@(CanonicalVar _ _) _) = Just $ subTop s t1
evalStep (CanonicalApp t1@(CanonicalAbs _ _) t2 ext) = Just $ (CanonicalApp t1 (fromJust $ evalStep t2) ext)
evalStep (CanonicalApp t1 t2 ext) = Just $ (CanonicalApp (fromJust $ evalStep t1) t2 ext)
evalStep _ = Nothing

eval :: NamelessTerm -> NamelessTerm
eval t =
  case evalStep t of
    Just t' -> eval t'
    Nothing -> t

--testEval = do
--  let idfunc = toCanonical  (Abs (Var "x") (Var "x"))
--  let tru = (Abs (Var "t") (Abs (Var "f") (Var "t")))
--  let fls = (Abs (Var "t") (Abs (Var "f") (Var "f")))
--  let tst = (Abs
--                (Var "l")
--                (Abs
--                    (Var "m")
--                    (Abs
--                        (Var "n")
--                        (App
--                          (App
--                            (Var "l")
--                            (Var "m")
--                          )
--                          (Var "n")
--                        )
--                    )
--                )
--            )
--  putStr "[Test Eval] id: "
--  print (eval idfunc)
--  putStr "[Test Eval] id id: "
--  print (eval (CanonicalApp idfunc idfunc))
--  putStr "[Test Eval] test true x y: "
--  print (eval . toCanonical $ (App (App (App tst tru) (Var "x")) (Var "y")))
--  putStr "[Test Eval] test fls x y: "
--  print (eval . toCanonical $ (App (App (App tst fls) (Var "x")) (Var "y")))
testCanonicalize :: IO ()
testCanonicalize = do
    putStr "[Test Canonical] x.x: "
    print $ toCanonical (Abs (Var "x") (Var "x"))
    putStr "[Test Canonical] x x: "
    print $ toCanonical (App (Var "x") (Var "x"))
    putStr "[Test Canonical] x y: "
    print $ toCanonical (App (Var "x") (Var "y"))
    putStr "[Test Canonical] x.x x: "
    print $ toCanonical (App (Abs (Var "x") (Var "x")) (Var "y"))
    putStr "[Test Canonical] x.y.x: "
    print $ toCanonical (Abs (Var "x") (Abs (Var "y") (Var "x")))
    putStr "[Test Canonical] x.y.y: "
    print $ toCanonical (Abs (Var "x") (Abs (Var "y") (Var "y")))
    putStr "[Test Canonical] shift 2 \\.\\. 1 (0 2): "
    print $ shift 0 2 $ toCanonical (Abs (Var "x") (Abs (Var "y") (App (Var "x") (App (Var "y") (Var "z")))))
--  putStr "[Test Canonical] Var x: "
--  print $ runState (canonicalize (Var "x")) []
--  putStr "[Test Canonical] x.x: "
--  print $ runState (canonicalize (App (Var "x") (Var "x"))) []
--  putStr "[Test Canonical] x.y.x: "
--  print $ runState (canonicalize (Abs (Var "x") (Abs (Var "y") (Var "x")))) []
--  putStr "[Test Canonical] x.y.y"
--  print $ runState (canonicalize (Abs (Var "x") (Abs (Var "y") (Var "y")))) []
